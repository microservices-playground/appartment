package com.study.micro.appartment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppartmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppartmentApplication.class, args);
    }
}
